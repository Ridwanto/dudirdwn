#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int JumlahAnak = 0;
    float GajiKotor = 0.0, Tunjangan = 0.0, PersenTunjangan = 0.0, PersenPotongan = 0.0;
    float Potongan = 0.0;
    float GajiBersih;

    PersenTunjangan = 0.2;
    PersenPotongan = 0.05;
    printf("Gaji Kotor ? ");
    scanf("%f", &GajiKotor);
    printf("Jumlah Anak ? ");
    scanf("%d", &JumlahAnak);

    if (JumlahAnak > 2) {
        PersenTunjangan = 0.3;
        PersenPotongan = 0.07;
    }

    Tunjangan = PersenTunjangan * GajiKotor;
    Potongan = PersenPotongan * GajiKotor;
      GajiBersih = GajiKotor - Potongan;

    if (JumlahAnak <= 2) {
        GajiBersih = GajiKotor;
    }

    printf("Besar Tunjangan = Rp %10.2f\n", Tunjangan);
    printf("Besar Potongan = Rp %10.2f\n", Potongan);
    printf("Besar Gaji Bersih = Rp %10.2f\n", GajiBersih);

return 0;
}
